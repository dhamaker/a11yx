---
title: Search widget
layout: page
---
<style>
label {display:block;}

main {
  width:185%;
  max-width:auto;
  overflow-x:hidden;
}
article{
  width:100vw;
  padding-bottom:2em;
}
.sample {
  max-width:85%;
}

main > nav {margin-left:1em;padding-top:0;}

</style>

<h2>Bad names</h2>
<div class="sample">
  <form action="#" role="search">
      <p>
          <!--<label id="q0Label" for="q0">Search</label>-->
          <input aria-label="Search" id="q0" name="q0" type="search" placeholder="Search" title="search">
          <button id="q0Button" type="submit">Search</button>
      </p>
  </form>
</div>

<h2>Site search</h2>
<h3>1. Basic widget, all names visible</h3>
<div class="sample">
  <form action="#" role="search" aria-labelledby="q1Section">
    <h4 id="q1Section">Site</h4>
      <p>
          <label id="q1Label" for="q1">Keyword</label>
          <input id="q1" name="q1" type="search">
          <button id="q1Button" type="submit">Search</button>
      </p>
  </form>
</div>

<h3>2. Hide heading</h3>
<div class="sample">
  <form action="#" role="search" aria-label="Site">
      <p>
          <label id="q2Label" for="q2">Keyword</label>
          <input id="q2" name="q2" type="search">
          <button id="q2Button" type="submit">Search</button>
      </p>
  </form>
</div>

<h3>3. Image label</h3>
  <div class="sample">
  <form action="#" role="search" aria-label="Site">
    <p>
      <label for="q3" style="display:inline;"><img alt="Keyword" src="ic_search.png"></label>
      <input id="q3" name="q3" type="search">
      <button type="submit">Search</button>
    </p>
  </form>
</div>

<h3>4. Hide button</h3>
<div class="sample">
  <form action="#" role="search" aria-label="Site">
      <p>
          <label for="q5" style="display:inline"><img alt="Keyword" src="ic_search.png"></label>
          <input id="q5" name="q5" type="search">
          <button hidden type="submit">Search</button>
      </p>
  </form>
</div>

<h2>Collection search</h2>

<h3>1. All visible</h3>
<div class="sample">
  <form action="#" role="search" aria-labelledby="q6Heading">
    <h3 id="q6Heading">Branch</h3>
      <p>
          <label id="q6Label" for="q6">Location</label>
          <input id="q6" name="q6" type="search" placeholder="address">
          <button  id="q6Button" type="submit">Find branch</button>
      </p>
  </form>
</div>


<h3>2. Heading hidden, title & button labelledby input</h3>
<div class="sample">
  <form action="#" role="search" aria-label="Account">
    <p>
      <label for="q4">Number</label>
      <input id="q4" name="q4" type="search" title="Enter four or more digits">
      <button id="q4Button" aria-labelledby="q4Button q4">Find account</button>
    </p>
  </form>
</div>

<h3>3. Image label</h3>
<div class="sample">
  <form action="#" role="search" aria-label="Customer">
      <p>
          <label id="q7Label" for="q7" style="display:inline"><img alt="Name" src="ic_search.png"></label>
          <input id="q7" name="q7" type="search" placeholder="last name">
          <button id="q7Button">Find customer</button>
      </p>
  </form>
</div>

<h3>4. Button labelledby input</h3>
<div class="sample">
  <form action="#" role="search" aria-label="Customer">
      <p>
          <label id="qLabel" for="qx">Name</label>
          <input id="qx" name="qx" type="search" value="Joe">
          <button aria-labelledby="qxButton qx" id="qxButton" type="submit">Find customer</button>
      </p>
  </form>
</div>

<h3>Remove button</h3>
<div class="sample">
  <form action="#" role="search" aria-label="Customer">
      <p>
          <label id="qyLabel" for="qy">Name</label>
          <input id="qy" name="qy" type="search" placeholder="Customer">
      </p>
  </form>
</div>
