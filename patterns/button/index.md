---
title:  Buttons
layout: page
---
<p>Native HTML5 input types:</p>
<ul>
  <li>submit type</li>
  <li>button type</li>
  <li>switch - switch state</li>
  <li>checked state</li>
  <li>toggle or push button - pressed state</li>
  <li>popup button</li>
  <li>Details button - expand/collapse state</li>
</ul>
<style>
    p button {display:inline-block;}
    [aria-checked="true"], [aria-pressed="true"], [aria-expanded="true"] {
        color:#fff;
        background:#000;
    }
    [aria-checked="mixed"], [aria-pressed="mixed"] {
        background:#eee;
    }
</style>
<form action="#" method="get">
<p> 
    <button type="submit">Submit</button>
</p>
<p>
    <button type="button">Button</button>
</p>
<p>
    <button type="button" role="switch" aria-checked="true">Reading light</button>
    <button type="button" role="switch" aria-checked="false">Reading light</button>
</p>
<p>
    <button type="button" role="checkbox" aria-checked="true">Option</button>
    <button type="button" role="checkbox" aria-checked="false">Option</button>
    <button type="button" role="checkbox" aria-checked="mixed">Option</button>
</p>
<p>
    <button type="button" aria-pressed="true">Audio</button>
    <button type="button" aria-pressed="false">Audio</button>
    <button type="button" aria-pressed="mixed">Audio</button>
</p>
<p>
    <button type="button" aria-expanded="true" aria-controls="controlObj">Details</button>
    <button type="button" aria-expanded="false" aria-controls="controlObj">Details</button>
</p>
<p>
    <button type="button" aria-haspopup="listbox" aria-controls="controlObj">Popup</button>
    <button type="button" aria-haspopup="dialog" aria-controls="controlObj">Popup</button>
</p>
<p id="controlObj">Example controlled content</p>
</form>
