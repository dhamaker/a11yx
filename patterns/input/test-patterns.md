---
title : Input test patterns
layout : page
---

<style>
    form > p {
        margin:0 0 1.5rem 1.5rem;
    }
    p span:first-of-type {
        display:block;
        color:#888;
        font-size:.85rem;
        margin-bottom:.25em;
    }
</style> 
</style>
           <p>Explore input attributes using a <code>text</code> input. Screen readers may further qualify the input type, but the basic pattern is similar for all fields that suport keyboard entry.</p>
            <form action="#">
                <p>
                    <span>basic</span>
                    <label for="t0">My name</label>  
                    <input id="t0" type="text" value="">
                </p>
                <p>
                    <span>basic + placehodler</span>
                    <label for="t1">My name</label>  
                    <input id="t1" type="text" value="" placeholder="my placeholder">
                </p>
                <p>
                    <span>basic + title</span>
                    <label for="t2">My name</label>  
                    <input id="t2" type="text" value="" title="my title">
                </p>
                <p>
                    <span>basic + description (weak support)</span>
                    <label for="t4">My name</label>  
                    <input id="t4" type="text" value="" aria-description="property description">
                </p>
                <p>
                    <span>basic + describedby</span>
                    <label for="t3">My name</label>  
                    <input id="t3" type="text" value="" aria-describedby="t3Helper">
                    <span id="t3Helper" class="hint">my helper text</span> 
                </p>
                <p>
                    <span>basic + describedby + html disabled</span>
                    <label for="t5">My name</label>  
                    <input id="t5" type="text" value="" disabled aria-describedby="t5Helper">
                    <span id="t5Helper" class="hint">my helper text</span> 
                </p>
                <p>
                    <span>basic + describedby + aria disabled</span>
                    <label for="t6">My name</label>  
                    <input id="t6" type="text"  value="" aria-disabled="true" aria-describedby="t6Helper">
                    <span id="t6Helper" class="hint">my helper text</span> 
                </p>
                <p>
                    <span>basic + describedby + html required </span>
                    <label for="t7">My name</label>  
                    <input id="t7" type="text" value="" required aria-describedby="t7Helper">
                    <span id="t7Helper" class="hint">my helper text</span> 
                </p>
                <p>
                    <span>basic + describedby + aria required</span>
                    <label for="t8">My name</label>  
                    <input id="t8" type="text"  value="" aria-required="true" aria-describedby="t8Helper">
                    <span id="t8Helper" class="hint">my helper text</span> 
                </p>
                <p>
                    <span>basic + placeholder + title + describedby + aria required + autocomplete </span>
                    <label for="t9">My name</label>  
                    <input id="t9" type="text" name="name" value="" aria-required="true" autocomplete="on" placeholder="my placeholder" title="my title" aria-describedby="t9Helper">
                    <span id="t9Helper" class="hint">my helper text</span> 
                </p>
            </form>
