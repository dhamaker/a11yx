---
title:  Input
layout: page
---
<p>Input without related form</p>
  <p>
    <label>
      Search
      <input type="search" id="keyword" value="">
    </label>
  </p>
<p>Native HTML5 input types:</p>
<ul>
  <li>Accessibility</li>
  <li>Mobile dynamic keyboard</li>
  <li>Auto complete</li>
  <li>Client-side validation</li>
  <li>Input masking</li>
</ul>

<form action="#" method="get" class="sample">

  <p>
    <label>
      Text
      <input type="text" id="text" value="">
    </label>
  </p>
  <p>
    <label>
      Password
      <input type="password" id="password" value="">
    </label>
  </p>
  <p>
    <label>
      Email
      <input type="email" id="email" value="" placeholder="your placeholder" title="your title" autocomplete="email">
    </label>
  </p>
  <p>
    <label>
      Telephone
      <input type="tel" id="mobile" value="" aria-required="true" aria-disabled="true" autocomplete="tel">
    </label>
  </p>
  <p>
    <label>
      URL
      <input type="url" id="url" value="" required>
    </label>
  </p>
  <p>
    <label>
      Spinner
      <input type="number" id="counter" value="" min="0" max="12">
    </label>
  </p>
  <p>
    <label>
      Slider
      <input type="range" name="slider" id="slider" value="0" min="-100" max="100" step="10">
    </label>
    <output for="slider"></output>
  </p>

  <p>
    <label>
      Date
      <input type="date" id="date" value="">
    </label>
  </p>
  <p>
    <label>
      Time
      <input type="time" id="time" value="">
    </label>
  </p>
  <p>
    <label>
      Search
      <input type="search" id="search" value="">
    </label>
  </p>
  <p>
    <label for="combo">Combobox</label>
    <input id="combo" list="list1">
    <datalist id="list1">
      <option>aardvark</option>
      <option>absolute</option>
      <option>abscond</option>
      <option>admit</option>
      <option>adrift</option>
      <option>aesop</option>
      <option>affect</option>
      <option>babble</option>
      <option>babar</option>
    </datalist>
  </p>
  <p>
    <label for="select">Select</label>
    <select id="select" aria-reqiured="true">
      <option value="0">Select One</option>
      <option value="1">January</option>
      <option value="2" selected>February</option>
      <option value="3">March</option>
      <option value="4">April</option>
      <option value="5">May</option>
      <option value="6">June</option>
      <option value="7">July</option>
    </select>
  </p>
  <fieldset>
    <legend>Are you ok?</legend>
    <label><input name="emotion" type="radio" value="Well" checked>Yes</label>
    <label><input name="emotion" type="radio" value="Unwell">No</label>
  </fieldset>
  <fieldset>
    <legend>Spices</legend>
    <label><input type="checkbox" value="salt">Salt</label>
    <label><input type="checkbox" value="pepper">Pepper</label>
    <label><input type="checkbox" value="curry">Curry</label>
  </fieldset>
</form>
