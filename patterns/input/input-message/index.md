---
title: Input messages
layout: page
---

<style>
    .helper-text {color:#999;}
    .error-message {color:#cc0000;}
    label, input, .helper-text {display:block;}
    input[aria-invalid="true"] {
        border: 2px solid #cc0000;
    }

    input ~ .error-message {display:none}
    input[aria-invalid="true"] ~ .error-message {display:block}
    input[aria-invalid="true"] ~ .helper-text {display:none}
</style>
<form action="#">
    <p>
        <label for="n1">Simple</label>
        <input id="n1" type="text" value="" required aria-describedby="n1help"> 
        <span id="n1help" class="helper-text">Text string</span>
    </p>
    <p>
        <label for="p1">email, polite hint</label>
        <input id="p1" type="email" value="" aria-describedby="p1help" onblur="inlineValidation(this);"> 
        <span id="p1help" class="helper-text">Enter a valid email address</span>
        <span id="p1err" class="error-message" aria-live="polite" aria-atomic="true">Invalid email address</span>
    </p>
    <p>
        <label for="a1">URL, assertive hint</label>
        <input id="a1" type="url" value="" aria-describedby="a1help" onblur="inlineValidation(this);">  
        <span id="a1help" class="helper-text">Enter a valid URL</span>
        <span id="a1err" class="error-message" aria-live="assertive" aria-atomic="true">Invalid URL</span>
    </p>
    <p>
        <label for="n2">Simpler</label>
        <input id="n2" type="text" value="" aria-describedby="n2help"> 
        <span id="n2help" class="helper-text">Optional information</span>
    </p>

    <button type="submit">Save</button>

    <script>
        function inlineValidation(e){
            status=e.validity.valid;
            if (!e.validity.valid){
                msg=e.id + "err"
                e.setAttribute("aria-describedby",msg)
                e.setAttribute("aria-invalid","true")
            } else {
                msg=e.id + "help"
                e.setAttribute("aria-describedby",msg)
                e.removeAttribute("aria-invalid")                    
            }
        }
    </script>
</form>
