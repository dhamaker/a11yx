---
title:  Link
layout: page
---
<style>
    li {margin:1rem 0;}
    dialog {
        border: 1px solid #666;
        border-radius: 6px;
        min-width:20rem;
        padding:0;
    }
    dialog header h1 {
        margin:0;
        padding:0;
        font-size:1rem;
    }
    dialog header {
        padding: .5rem;
        border-bottom: 1px solid #666;
        margin-bottom:1rem;
    }
    dialog main {
        padding: 0 .5rem 1rem .5rem;
    }
</style>

<p>Requirement.  Control labels must be clear, conspicuous, and use plain english.</p>
<ol>
    <li><a href="https://example.com/plain.html">Simple</a></li>
    <li><a aria-current="page" href="links.html">Current</a></li>
    <li><a href="document-image.jpg" download="your-document-copy.jpg" >Download</a></li>
    <li><a rel="previous" href="https://example.com/previous.html">Previous</a></li>
    <li><a rel="next" href="https://example.com/next.html">Next</a></li>
    <li><a rel="external" href="https://example.com/external.html">External</a></li>
    <li><a rel="help" href="https://example.com/help.html">Help</a></li>
    <li><a href="tel:5103884691">Call</a></li>
    <li><a href="mailto:example@example.com">Email</a></li>
    <li><a target="_blank" href="https://example.com/new-window.html">New window</a></li>
    <li><button>No action</button></li>
    <li><button onclick="myModal.showModal();">Child context</button></li>
</ol>
<dialog id="myModal">
    <header>
        <h1>Child context</h1>
    </header>
    <main>
        <p>This is a native modal dialog. Press <code>esc</code> to close.</p>
        <button onclick="myModal.close()" formmethod="dialog">Close</button>
    </main>
</dialog>

<details closed>
    <summary>Results</summary>
    <p>Links announce like "link, Clear and conspicuous", "visited, link, Clear and conspicuous", "current page, visited link, Clear and conspicuous."</p>
    <p>Buttons announce like "Clear and conspicuous, button."</p>
    <ol>
        <li>plain link</li>
        <li>link with aria-current="page"</li>
        <li>download link</li>
        <li>rel="previous" link</li>
        <li>rel="next" link</li>
        <li>rel="external" link</li>
        <li>rel="help" link.  Expect help cursor</li>
        <li>href="tel:..." link. Create a call</li>
        <li>href="mailto:..." link.  Create an email</li>
        <li>targer="_blank" link. Create new browsing window</li>
        <li>button</li>
        <li>modal dialog button</li>
    </ol>
</details>
