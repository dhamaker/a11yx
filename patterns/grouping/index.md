---
title: Grouping content
layout: page
---
<h2>Overview</h2>
<p>Simple seems to win.  Using one &lt;p&gt; per idea, the "(multiple p)" version, is a winner.  (p + div) version announces and navigates similarly. However, (multipl p) reads cleaner with VO virtual cursor, virtual cursor matches reading with (muliple p).  On (p+div), iOS virtual cursor does not synch with reading.</p>

<p>The other approaches add noise without significant information.  (* named list) identifies list purpose and size and is OK but doesn't seem to improve clarity. (p + has excessive markup that makes touch navigation more difficult.  And (unamed list), (h) increase noise</p>

<h2>Approaches</h2>
<article>
    <h3>Confirm Transaction (multiple p). WINNER</h3>
    <p>Do these details look accurate?</p>
    <p>Account: Checking # ****1234</p>
    <p>Amount: $500.00</p>
    <p>Date: 11/1/2022</p>
    <button>Approve</button>
    <button>Deny</button>
</article>

<article>
    <h3>Confirm Transaction (p + span)</h3>
    <p>
        <span>Do these details look accurate?</span>
        <span>
            <span>Account: Checking # ****1234.</span>
            <span>Amount: $500.00.</span>
            <span>Date: <date>11/1/2022</date>.</span>
        </span>
    </p>
    <button>Approve</button>
    <button>Deny</button>
</article>
<article>
    <h3>Confirm Transaction (p + div)</h3>
    <p>Do these details look accurate?</p>
    <p>
        <div>Account: Checking # ****1234</div>
        <div>Amount: $500.00</div>
        <div>Date: 11/1/2022</div>
    </p>
    <button>Approve</button>
    <button>Deny</button>
</article>


<article>
    <h3>Confirm Transaction (un-named list)</h3>
    <p>Do these details look accurate?</p>
    <ul>
        <li>Account: Checking # ****1234</li>
        <li>Amount: $500.00</li>
        <li>Date: 11/1/2022</li>
    </ul>
    <button>Approve</button>
    <button>Deny</button>
</article>

<article>
    <h3>Confirm Transaction (named list)</h3>
    <p>Do these details look accurate?</p>
    <p id="det1">Transaction details</p>
    <ul aria-labelledby="det1">
        <li>Account: Checking # ****1234</li>
        <li>Amount: $500.00</li>
        <li>Date: 11/1/2022</li>
    </ul>
    <button>Approve</button>
    <button>Deny</button>
</article>

<article>
    <h3>Confirm Transaction (custom named list)</h3>
    <p>Do these details look accurate?</p>
    <style>
        ul.naked {list-style:none;margin-left:0;padding-left:0;}
    </style>
    <ul aria-label="Transaction details" role="list" class="naked">
        <li>Account: Checking # ****1234</li>
        <li>Amount: $500.00</li>
        <li>Date: 11/1/2022</li>
    </ul>
    <button>Approve</button>
    <button>Deny</button>
</article>

<article>
    <h3>Confirm Transaction (h)</h3>
    <p>Do these details look accurate?</p>
    <h4>Account</h4>
    <p>Checking # ****1234</p>
    <h4>Amount</h4> 
    <p>$500.00</p>
    <h4>Date</h4>
    <p>11/1/2022</p>
    <button>Approve</button>
    <button>Deny</button>
</article>

<article>
    <h3>Confirm Transaction (emphasis)</h3>
    <p>
        <span>Account</span>,
        <em>Account</em>,
        <i>Account</i>,
        <strong>Account</strong>,
        <b>Account</b>,
    </p>
    <p>
        <span>Do these details look accurate?</span>
        <span>
            <span><em>Account</em>: Checking # ****1234.</span>
            <span><strong>Amount</strong>: $500.00.</span>
            <span>Date: 11/1/2022.</span>
        </span>
    </p>
    <button>Approve</button>
    <button>Deny</button>
</article>

