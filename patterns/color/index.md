---
title:  Color contrast
layout: page
---

<style>
    body {font-family:sans-serif;color:rgb(0,0,0);}
    section {background-color:#fff;;}

    .er {color:rgb(118 118 118);}

    .status {color:rgb(0 138 25)}
    .er .status {color:rgb(0 138 25)}

    a {text-decoration:none;color:rgb(0, 110, 251)}
    a:hover {text-decoration:underline;}
    .er a {}

    li {
        margin:1em 0;
    }
    button {
        margin:0rem 1rem;
    }

    .styled {
        font-family: sans-serif;
        font-size:1rem;        padding:.5rem 1rem;
        width:8rem;
        display:block;
        background-color:rgb(240,240,240);
        border:1px solid rgb(80,80,80);
        border-radius:3px;
        color:rgb(0,0,0);
    }

    .styled:focus-visible, .styled:focus, .styled:focus-within {
       /*border:2px solid #fff;
       outline:2px solid rgb(0,0, 255);*/
    }

    .styled:active {
        box-shadow: #333 0px 0px 3px;
        background: #eee;
    }

    .b2 {
        border:2px solid rgb(255, 255, 255);
        border-radius:3px;
        background-color: rgb(255, 255, 255);
    }
    
    .b2:hover {
        border-color:rgb(240, 240, 240);
        background-color:rgb(240, 240, 240);
    }
    .b2:active {
        border-color:rgb(220, 220, 220);
        background-color:rgb(220, 220, 220);
    }
    
    .b3:hover {
        background-color:rgb(0,0,0);
        color:rgb(240,240,240);
    }
    .b3:active {
        background-color:rgb(0,0,200);
    }

    .b4 {
        background: linear-gradient(180deg, rgba(9,9,121,0.1) 05%, rgba(9,9,121,0.2) 35%, rgba(9,9,121,0.1) 100%);
    }

    .b5 {
        background: linear-gradient(180deg, rgba(9,9,121,0.8) 05%, rgba(9,9,121,0.7) 35%, rgba(9,9,121,0.8) 100%);
    }
    .b5:active {
        background: linear-gradient(180deg, rgba(9,9,121,0.2) 05%, rgba(9,9,121,0.1) 50%, rgba(9,9,121,0.2) 100%);
    }
</style>

<p>Color contrast test examples</p>
<section>
    <h2>Text Contrast</h2>
    <ol>
        <li class="yi">The quick brown fox jumped over the lazy dog.  <span class="status">(Pass)</span></li>
        <li class="er">The quick brown fox jumped over the lazy dog.  <span class="status">(Pass)</span></li>
        <li class="yi">The quick <a href="#">brown fox</a> jumped over the lazy dog.</li>
        <li class="er">The quick <a href="#">brown fox</a> jumped over the lazy dog.</li>
    </ol>
</section>
<section>
    <h2>Non-Text Contrast</h2>
    <ol>
        <li><button class="b1" onclick="alert('Button state is active.')">Test</button></li>
        <li>
            <button class="b2 styled" onclick="alert('Button state is active.')">Test</button>
        </li>
        <li>
            <button class="b3 styled" onclick="alert('Button state is active.')">Test</button>
        </li>
        <li>
            <button class="b4 styled" onclick="alert('Button state is active.')">Test</button>
        </li>
        <li>
            <button class="b5 styled" onclick="alert('Button state is active.')">Test</button>
        </li>
    </ol>
    
</section>
<details>
    <summary>Expected results</summary>
    <p>Text contrast</p>
    <ol>
        <li>Pass. Text/base, 25:1.  Status/base, 4.52:1.  Status/Text, 4.64:1.</li>
        <li class="er">Pass. Text/base, 4.54.1.  Status/base, 4.52:1.  <mark>Status/Text, 1:1.  Status contrast fails BUT does not use color alone; therefore, pass.</mark></li>
        <li>Pass. Text/base, 25:1.  Link/base, 4.54:1.  Link/Text, 4.62:1. Pass, uses color and contrast to distinguish text from interactive control.</li>
        <li class="er"><mark>Fail</mark>. Text/base, 4.54:1.  Link/base, 5.54:1.  <mark>Link/Text, 1:1</mark>. Fails because only color is used to distinguish text from interactive control.</li>
    </ol>

    <p>Non-text contrast</p>
    <ol>
        <li>Pass in normal, hover, focus and active states. Because default browser styles are not edited.</li>
        <li>Pass in normal, hover, focus, and active states.  Color is not used to identify control, so no background and border states do not need to contrast with base.</li>
        <li><mark>Fail</mark>.  Normal: pass.  Hover: pass.  Focus: pass.  <mark>Active: fail</mark>; text does not have sufficient contrast.</li>
        
        <li>Pass.  Normal: pass. Hover: pass.  Focus: pass. Active: pass. BUT Axe DevTools automated give false negative because of transparent background. </li>
        <li><mark>Fail</mark>.  <mark>Normal: fail</mark>. Hover: pass.  <mark>Focus: fail</mark>.  Active: pass.  BUT Axe DevTools automated give false positive because of transparent background.</li>
    </ol>

</details>
