---
title:  Date
layout: page
---
<p>Native HTML5 date types:</p>
<form action="#" method="get" class="sample">
  <p>
    <label for="text">Start date</label>
    <input type="text" id="text" value="6/17/2021">
    <div>text</div>
  </p>
  <p>
    <label for="date">Start date</label>
    <input type="date" id="date" value="2021-06-07">
    <div>date</div>
  </p>
  <p>
    <label for="combobox">Start date</label>
    <input type="text" role="combobox" id="combobox" value="6/17/2021" inputmode="numeric" aria-haspopup="dialog" aria-expanded="false" aria-controls="cbDialog" aria-autocomplete="none">
    <div id="cbDialog">Picker dialog</div>
    <div>custom</div>
  </p>
</form>
