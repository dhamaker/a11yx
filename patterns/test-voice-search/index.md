---
title: Search form with implied submit
layout: page
---
<style>
    #results {display:none;margin-top: 3rem;}
    #results:target {display:block;}
</style>
<p>Search form with one input and no submit button.  HTML is valid and form will submit by pressing <code>Enter</code> key.</p>
<p>Question:  How to submit this form using voice input?</p>
<p>Success: Results section displays.</p>
<form action="#results" role="search" aria-label="Site">
    <label>
        Keyword&nbsp;
        <input id="q4" name="q4" type="search">
    </label>
</form>
<section id="results">
    <h2>Results</h2>
    <p>Query results</p>
    <a href="#">Reset search</a>
</section>
