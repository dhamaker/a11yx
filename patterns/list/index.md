---
title: Lists
layout: page
---
<style>
    body, input, button {
        font-family:sans-serif;
        font-size:1rem;
    }

    ul {
        list-style-position: inside;
    }

    .liststyle {
        list-style:none;
    }
    .liststyletype {
        list-style-type:"";
    }
    .flex-layout {
        display: flex;
        flex-wrap : wrap;
        gap:.5rem;

    }

    .flex-layout li {
        padding:.5rem;
        background: #ddd;
        border-radius:3px;
    }

    .grid-layout {
        display:grid;
        grid-template-columns: repeat(3, 1fr);
        grid-template-rows: auto;
        grid-template-areas:
            "item . .";

    }

    .grid-layout li:first-of-type {
        grid-column-start: 2;

    }

    .grid-layout li {
        padding:.5rem;
        background: #ddd;
        border-radius:3px;

</style>

<p><code>list-style:none</code> and <code>list-style-type:none</code> break list semantics on some UA.  If you want to ensure list structure is announced, you must declare an explicit role: <code>&lt;ul role="list"&gt;</code></p>

<h2>Plain</h2>
<p>Implicit role</p>
<ul>
    <li>Twins</li>
    <li>White Sox</li>
    <li>Tigers</li>
    <li>Royals</li>
    <li>Guardians</li>
</ul>

<p>Implicit role AND <code>list-style:none</code></p>
<ul class="liststyle">
    <li>Twins</li>
    <li>White Sox</li>
    <li>Tigers</li>
    <li>Royals</li>
    <li>Guardians</li>
</ul>

<p>Explicit role AND <code>list-style:none</code></p>
<ul role="list" class="liststyle">
    <li>Twins</li>
    <li>White Sox</li>
    <li>Tigers</li>
    <li>Royals</li>
    <li>Guardians</li>
</ul>


<h2>Flexbox</h2>
<p>Implicit role</p>
<ul class="flex-layout">
    <li>Twins</li>
    <li>White Sox</li>
    <li>Tigers</li>
    <li>Royals</li>
    <li>Guardians</li>
</ul>

<p>Implicit role AND <code>list-style:none</code></p>
<ul class="flex-layout liststyle">
    <li>Twins</li>
    <li>White Sox</li>
    <li>Tigers</li>
    <li>Royals</li>
    <li>Guardians</li>
</ul>

<p>Implicit role AND <code>list-style-type:""</code></p>
<ul class="flex-layout liststyletype">
    <li>Twins</li>
    <li>White Sox</li>
    <li>Tigers</li>
    <li>Royals</li>
    <li>Guardians</li>
</ul>

<p>Explicit role</p>
<ul role="list" class="flex-layout">
    <li>Twins</li>
    <li>White Sox</li>
    <li>Tigers</li>
    <li>Royals</li>
    <li>Guardians</li>
</ul>

<p>Explicit role AND <code>list-style:none</code></p>
<ul role="list" class="flex-layout liststyle">
    <li>Twins</li>
    <li>White Sox</li>
    <li>Tigers</li>
    <li>Royals</li>
    <li>Guardians</li>
</ul>

<h2>Grid</h2>
<p>Implicit role</p>
<ul class="grid-layout">
    <li>Twins</li>
    <li>White Sox</li>
    <li>Tigers</li>
    <li>Royals</li>
    <li>Guardians</li>
</ul>

<p>Implicit role AND <code>list-style:none</code></p>
<ul class="grid-layout liststyle">
    <li>Twins</li>
    <li>White Sox</li>
    <li>Tigers</li>
    <li>Royals</li>
    <li>Guardians</li>
</ul>

<p>Explicit role</p>
<ul role="list" class="grid-layout">
    <li>Twins</li>
    <li>White Sox</li>
    <li>Tigers</li>
    <li>Royals</li>
    <li>Guardians</li>
</ul>

<p>Explicit role AND <code>list-style:none</code></p>
<ul role="list" class="grid-layout liststyle">
    <li>Twins</li>
    <li>White Sox</li>
    <li>Tigers</li>
    <li>Royals</li>
    <li>Guardians</li>
</ul>
