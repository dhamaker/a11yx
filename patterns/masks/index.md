---
title:  Masking
layout: page
---

Masking occurs when raw data values are transformed into secure and/or formatted string for presentation.

Sample privacy and pattern masks for input and text.

<form action="#">
    <h2>Standard input mask</h2>
    <p>There is a standard secure mask experience, password.  It is a common experience with predictable visual and audio experiences that help protect users.</p>
    <p>
        <label for="pwd">Password</label>
        <input type="password" id="pwd" value="1234">
    </p>
    <details>
        <summary>Screen reader (on focus):</summary>
        <ul>
            <li>
                <div>Voiceover+iOS: Password, four characters, secure text field.  Double tap to edit.</div>
            </li>
            <li>
                <div>Voiceover: Password. Four charcters content selected,  Password,  secure edit text.</div>
                <div>You are currently on a text field.  To enter text into this field, type. This is as secure text field. Text typed into this field will not be displayed and will not be spoken.</div>
            </li>
        </ul>
    </details>
    
    <h2>Custom input mask</h2>
    <p>Authors provide equivalent secure experiences when creating custom secure masks.  Forgetting the audio experience, creates security and accessibility issues.</p>
    <p>ARIA enhanced values {label : <var id="secureLbl">, Showing last four digits</var>; description : <var id="secureMsg">This is a secure text field.</var>; email label : <var id="secureLblEmail">, showing partial value</var>,}</p>
    
    <h3>Example 1: Credit card number</h3>
    <p>
        <label for="cc1">Credit card number</label>
        <input type="text" id="cc1" value="•••• •••• •••• 4421">
    </p>
    <details>
        <summary>Screen reader (on focus)</summary>
        <ul>
            <li>Voiceover + iOS: Credit card number, four bullets four bullets four bullets 4421. Double tap to edit.</li>
        </ul>
    </details>
    <p>
        <label id="ccA">Credit card number</label>
        <input type="text" aria-labelledby="ccA secureLbl" aria-describedby="secureMsg" id="cc3" value="••••&nbsp;••••&nbsp;••••&nbsp;4423">
    </p>
    <details>
        <summary>ARIA enhanced screen reader (on focus)</summary>
        <ul>
            <li>Voiceover + iOS: Credit card number, showing last four digits, four bullets four bullets four bullets 4423. This is a secure text field.  Double tap to edit</li>
        </ul>
    </details>
    <p>
        <label id="ccBA">Credit card number</label>
        <input type="text" aria-labelledby="ccBA secureLbl" aria-describedby="secureMsg" id="cc4" value="••••&nbsp;&hyphen;&nbsp;••••&nbsp;&hyphen;&nbsp;••••&nbsp;&hyphen;&nbsp;4423">
    </p>
    
    <h3>Example 2: Tax ID</h3>
    <p>
        <label for="tin1">Tax ID</label>
        <input type="text" id="tin1" value="•••-••-4401">
    </p>
    <details>
        <summary>Screen reader (on focus)</summary>
        <ul>
            <li>Voiceover + iOS: Tax ID, three bullets bullet bullet minus 4401. Double tap to edit.</li>
        </ul>
    </details>
    <p>
        <label id="tinA">Tax ID</label>
        <input aria-labelledby="tinA secureLbl" aria-describedby="secureMsg" type="text" id="tin2" value="•••&nbsp;&hyphen;&nbsp;••&nbsp;&hyphen;&nbsp;4302">
        <span>slower read</span>
    </p>
    <details>
        <summary>ARIA enhanced screen reader (on focus):</summary>
        <ul>
            <li>Voiceover + iOS: Tax ID, showing last four digits, three bullets bullet bullet 4402. This is a secure text field.  Double tap to edit. </li>
        </ul>
    </details>
    <p>
        <label for="tin3">Tax ID</label>
        <input type="text" id="tin3" value="•••&hyphen;••&hyphen;4302">
    </p>
    
    <h3>Example 3: Email</h3>
    <p>
        <label for="email1">email address</label>
        <input type="email" id="email1" value="t••••j@example.com">
    </p>
    <details>
        <summary>Screen reader (on focus)</summary>
        <ul>
            <li>Voiceover + iOS: Email, t four bullets j@example.com. Double tap to edit.</li>
        </ul>
    </details>
    <p>
        <label id="emailA">email address</label>
        <input aria-labelledby="emailA secureLblEmail" aria-describedby="secureMsg" type="email" id="email2" value="t••••j@example.com">
    </p>
    <details>
        <summary>ARIA enhanced screen reader (on focus):</summary>
        <ul>
            <li>Voiceover + iOS: Email, showing partial value, t four bullets j@example.com. This is a secure text field.  Double tap to edit.</li>
        </ul>
    </details>

    <h3>Example 4: Account number</h3>
    <p>
        <label id="acctA">Account number</label>
        <input aria-labeledby="acct" type="text" id="acct1" value="•••••-4321">
    </p>
    <details>
        <summary>Screen reader (on focus):</summary>
        <ul>
            <li>
                <div>Voiceover iOS:  Account number, five bullets minus 4321, text field.  Double tap to edit.</div>
            </li>
            <li>
                <div>Voiceover Mac: bullet bullet bullet bullet bullet minus 4321 content selected, Account number, edit text.</div>
                <div>You are currently on a text field.  To enter text into this field, type.</div>
            </li>
        </ul>
    </details>

    <p>
        <label id="acctE">Account number</label>
        <input aria-labelledby="acctE secureLbl" aria-describedby="secureMsg" type="text" id="acct5" value="&bull;&bull;&bull;&bull;&bull;&nbsp;&hyphen;&nbsp;4320">
    </p>
    <details>
        <summary>ARIA enhanced screen reader (on focus):</summary>
        <ul>
            <li>
                <div>Voiceover iOS:  Account number, secure, five bullets 4320, text field.  This is a secure text field.  Double tap to edit.</div>
            </li>
            <li>
                <div>Voiceover (on focus): bullet bullet bullet bullet bullet 4320 content selected, Account number, Secure, edit text. </div>
                <div>This is a secure text field.  You are currently on a text field.  To enter text into this field, type.</div>
            </li>
        </ul>
    </details>

    <h3>Example 5: Currency</h3>
    <p>
        <label for="dollars">Amount</label>
        <input type="text" id="dollars" value="$4,300">
    </p>
    <p>
        <label for="cents">Amount</label>
        <input type="text" id="cents" value="$1,200.50">
    </p>
    <h3>Example 6: Phone number</h3>
    <p>
        <label for="phone">Phone number</label>
        <input type="text" id="phone" value="415-555-5000">
    </p>
    <p>
        <label for="phonetext">Phone text</label>
        <input type="text" id="phonetext" value="415-555-5000">
    </p>

</form>
<h2>Text masks</h2>
<p>SSN: ***-**-4321</p>
<p>CC: ****-****-****-4321</p>
<p>Account number: <b title="ending in 1234">*****-1234</b></p>
<p>Account number: <i title="ending in 1234">*****-1234</i></p>
