---
title: Toggle & button
layout: page
---
<h2>{{page.title}} (aria-pressed)</h2>
  <script>
    function togglePress (e) {
        if (e.ariaPressed=="false") {
            e.setAttribute("aria-pressed", "true");
        } else {
            e.setAttribute("aria-pressed", "false");
        }
    }
  </script>

  <h3>Champion - group by list</h3>
  <p>Javascript toggles the aria-pressed between true and false.   Pressed state is shown visually as old text using [aria-pressed="true"].</p>

  {% include_relative champion.html %}

<h3>Challenger - group by fielset and list</h3>
<p>Javascript toggles the aria-pressed between true and false.   Pressed state is shown visually as old text using [aria-pressed="true"].</p>

{% include_relative challenger.html %}

<section class="contenders">
  <h2>Contender archive</h2>
  {% include_relative toggle-button-fieldset.html %}
  {% include_relative relabel.html %}
  {% include_relative checkbox.html %}
</section>
