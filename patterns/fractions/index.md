---
title: Fractions
layout: page
---

## Vulgar fractions
Plain language fractions: one half, one third, etc. [Number Forms (Wikipedia)](https://en.wikipedia.org/wiki/Number_Forms)

**Test string (decimal encoding):** &#188; &#189; &#190; &#8528; &#8529; &#8530; &#8531; &#8532; &#8533; &#8534; &#8535; &#8536; &#8537; &#8538; &#8539; &#8540; &#8541; &#8542;
