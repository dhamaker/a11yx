---
title : Disclosure patterns
layout : page
---

## Failed nesting

<p>
   <input type="checkbox" id="consent0">
   <label for="consent0">I have read the <a href="https://example.com/terms.html">Terms and Conditions</a> and agree to follow the rules.</label>
</p>
